# COMPARATIVE META-ANALYSIS

Source code for manuscript "A comparative meta-analysis to assess efficacy of
decision support systems in reducing the incidence of
fungal diseases."
by E.Lázaro, D. Makowski, J. Martínez-Minaya and A. Vicent.

For questions, comments or remarks about the code please contact E. Lázaro (lazaro_ele@gva.es).

The code has been written using R version 3.5.1 (2018-07-02) (platform: x86_64-w64-mingw32/x64 (64-bit)) 

To reproduce the results presented in the manuscript, just run the main analysis file 
code>analyses_comparativemetaanalysis.R. 
